#ifndef __PROLOGUE_H__
#define __PROLOGUE_H__

#include <Arduino.h>
#include <stdint.h>

#define PROLOGUE_BLINK		50000

#define TICKS_PER_uS		5 	// TIM_DIV16

#define PROLOGUE_START		530
#define PROLOGUE_SYNC		(18 * PROLOGUE_START)
#define PROLOGUE_ZERO		(4 * PROLOGUE_START)
#define PROLOGUE_ONE		(8 * PROLOGUE_START)

#define PROLOGUE_TYPE_LEN	4
#define PROLOGUE_ID_LEN		8
#define PROLOGUE_BATT_LEN	1
#define PROLOGUE_BUTN_LEN	1
#define PROLOGUE_CHAN_LEN	2
#define PROLOGUE_TEMP_LEN	12
#define PROLOGUE_HUMD_LEN	8

#define PROLOGUE_MSG_LEN	(PROLOGUE_TYPE_LEN + \
							 PROLOGUE_ID_LEN + \
							 PROLOGUE_BATT_LEN + \
							 PROLOGUE_BUTN_LEN + \
							 PROLOGUE_CHAN_LEN + \
							 PROLOGUE_TEMP_LEN + \
							 PROLOGUE_HUMD_LEN)			

#define PROLOGUE_MSG_PULSES	(2 * (PROLOGUE_MSG_LEN + 1))

#define PROLOGUE_DEF_BURSTS	7

#define PROLOGUE_TEMP_SCALE	10

typedef struct
{
	uint8_t type : PROLOGUE_TYPE_LEN;
	uint8_t id : PROLOGUE_ID_LEN;
	uint8_t battery : PROLOGUE_BATT_LEN;
	uint8_t button : PROLOGUE_BUTN_LEN;
	uint8_t channel : PROLOGUE_CHAN_LEN;
	uint16_t temperature : PROLOGUE_TEMP_LEN;
	uint8_t humidity : PROLOGUE_HUMD_LEN;
}
prologue_msg_t;

typedef uint64_t prologue_bits_t;

void initPrologue (uint8_t pin, uint8_t channel);
uint64_t getPrologueBits (void);
void getPrologueBitString(char * buf, uint16_t len);
void sendPrologue (float temeperature, float humidity);
void blinkPrologue (void);

#endif 