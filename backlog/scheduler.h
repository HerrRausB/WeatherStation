#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include <iostream> 
#include <list> 
#include <iterator> 
using namespace std;

#include "I2CDEV.h"
#include "ScheduledItem.h"

class CScheduler : list<IScheduledItem>
{
	public:

		CScheduler (void);
		~CScheduler (void);

		void schedule (void);

	private:

};

#endif // !__SCHEDULER_H__