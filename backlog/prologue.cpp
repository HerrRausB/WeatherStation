#include "prologue.h"

// #define __DEBUG__
// #include "wsdebug.h"

//---------------------------------------
// internal affairs
//---------------------------------------

const uint32_t _ticksSync = PROLOGUE_SYNC * TICKS_PER_uS;
const uint32_t _ticksStart = PROLOGUE_START * TICKS_PER_uS;
const uint32_t _ticksZero = PROLOGUE_ZERO * TICKS_PER_uS;
const uint32_t _ticksOne = PROLOGUE_ONE * TICKS_PER_uS;
const uint8_t _msgPulses = PROLOGUE_MSG_PULSES;

bool _prologue_is_init = false,
	 _txon = false;

uint8_t _txpin = 0,
		_pinstate = 0,
		_txpulse = 0,
		_bursts = 0;

prologue_msg_t _msgbuf;
prologue_bits_t _msgbits, _bitmask;

uint32_t _lutpulses[PROLOGUE_MSG_PULSES];

void ICACHE_RAM_ATTR _tick(void);
void _nextBurst (void);
void _getPrologueMsgPulses (void);

void ICACHE_RAM_ATTR _tick(void)
{
	if (_txpulse < _msgPulses)
	{
		_pinstate = 1 - _pinstate;
		digitalWrite(_txpin, _pinstate);
		timer1_write (_lutpulses[_txpulse]);
		_txpulse++;
	}
	else
	{
		_bursts--;
		_nextBurst ();
	}
}

void ICACHE_RAM_ATTR _nextBurst (void)
{
	if (_bursts)
	{
		_txpulse = 0;
		_pinstate = 0;
		_tick ();
	}
	else
	{
		timer1_detachInterrupt();
		digitalWrite(_txpin, LOW);
		_txon = false;
	}
}

void _getPrologueMsgPulses ()
{
	uint8_t i;
	uint64_t bits = getPrologueBits(),
			 m = 1;

	_lutpulses[0] = _ticksStart;
	_lutpulses[1] = _ticksSync;
	for (i = 2; i < _msgPulses; i+=2)
	{
		_lutpulses[_msgPulses-i] = _ticksStart;
		_lutpulses[_msgPulses-i+1] = (bits & m) ? _ticksOne : _ticksZero;
		m <<= 1;
	}
}


//---------------------------------------
// API
//---------------------------------------

uint64_t getPrologueBits(void)
{
	uint64_t bits = 0;

	bits |= (uint64_t)_msgbuf.type;

	bits <<= PROLOGUE_ID_LEN;
	bits |= (uint64_t)_msgbuf.id;

	bits <<= PROLOGUE_BATT_LEN;
	bits |= (uint64_t)_msgbuf.battery;

	bits <<= PROLOGUE_BUTN_LEN;
	bits |= (uint64_t)_msgbuf.button;

	bits <<= PROLOGUE_CHAN_LEN;
	bits |= (uint64_t)_msgbuf.channel;

	bits <<= PROLOGUE_TEMP_LEN;
	bits |= (uint64_t)_msgbuf.temperature;

	bits <<= PROLOGUE_HUMD_LEN;
	bits |= (uint64_t)_msgbuf.humidity;

	return bits;
}

void getPrologueBitString(char *buf, uint16_t len)
{
	uint8_t i;
	uint64_t bits = getPrologueBits(),
			 m = 1;

	if (buf)
	{
		memset(buf, 0, len);
		for (i = 0; (i < (len - 1)) && (i < PROLOGUE_MSG_LEN); i++)
		{
			buf[PROLOGUE_MSG_LEN - i - 1] = (bits & m) ? '1' : '0';
			m <<= 1;
		}
	}
}

void initPrologue(uint8_t pin, uint8_t channel)
{
	if (!_prologue_is_init)
	{
		_txon = false;
		_txpin = pin;
		_msgbuf.type = 0x09;
		_msgbuf.channel = channel - 1;
		_msgbuf.id = 0x04;
		_msgbuf.battery = 0;
		_msgbuf.button = 0;
		_msgbuf.temperature = 0;
		_msgbuf.humidity = 0;

		pinMode(_txpin, OUTPUT);
		timer1_detachInterrupt();		// just in case...
		timer1_enable(TIM_DIV16, TIM_EDGE, TIM_SINGLE);
		_prologue_is_init = true;
	}
}

void sendPrologue(float temperature, float humidity)
{
	if (_prologue_is_init && !_txon)
	{
		_msgbuf.temperature = (uint16_t)(temperature * PROLOGUE_TEMP_SCALE);
		_msgbuf.humidity = (uint8_t)(((humidity * 10) + 5) / 10); // rounding

		_getPrologueMsgPulses ();

		_bursts = PROLOGUE_DEF_BURSTS;
		_txon = true;

		timer1_attachInterrupt(_tick); // Add ISR Function
		_nextBurst ();
	}
}