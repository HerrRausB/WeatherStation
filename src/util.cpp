#include <ESP8266WiFi.h>
#include "util.h"
#include <math.h>

char charbuf[256];

int32_t getRSSI(const char *target_ssid)
{
  byte available_networks = WiFi.scanNetworks();

  for (int network = 0; network < available_networks; network++)
  {
    if (strcmp(WiFi.SSID(network).c_str(), target_ssid) == 0)
    {
      return WiFi.RSSI(network);
    }
  }
  return 0;
}

float adjustFloat(float f, uint8_t d)
{
  int32_t adjust = (int32_t)(f * pow(10, d));

  return (float)((float)adjust / (float)pow(10, d));
}
