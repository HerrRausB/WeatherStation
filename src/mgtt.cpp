#include "weatherstation.h"

#define JSON_BUF_LEN	255

WiFiClient wlanc;
boolean _isinit = false;
char _wdcorrectiontopic[MAX_MQTT_TOPIC_LEN];

PubSubClient mqttc(wlanc);

void mqttInit(void);
boolean connected(void);
void handleMQTT(char *topic, byte *payload, unsigned int length);

void handleMQTT(char *topic, byte *payload, unsigned int length)
{
	uint32_t wdcorrection = 0;
	char buf[256];

	sprintf(buf, "Topic: %s", topic);
	wsdebug(buf);

	if (!strcmp(topic, _wdcorrectiontopic))
	{
		// convert the byte stream paylod to a uint32_t...
		for (unsigned int i = 0; i < length; i++)
		{
			wdcorrection *= 10;
			wdcorrection += (uint32_t)(payload[i] - '0');
		}
		setWDCorrection(wdcorrection);
	}

	if (!strcmp(topic, SUBS_TEMP_SENSOR_TOPIC))
	{
		char jsonbuf[JSON_BUF_LEN+1];

		memset(jsonbuf, 0, JSON_BUF_LEN+1);
		memcpy(jsonbuf, payload, (length < JSON_BUF_LEN) ? length : JSON_BUF_LEN);

		wsdebug(jsonbuf);

		//JSONVar json = JSON.parse(jsonbuf);

		//if (json.hasOwnProperty("temperature") &&
		//	json.hasOwnProperty("humidity"))
		//{
		//	sendGTWT03((double)json["temperature"], (double)json["humidity"], GTWT03_CHANNEL_EXT);
		//}
	}

}

void mqttInit()
{
	if (!_isinit)
	{
		mqttc.setServer("infrapi.shakespeare.lan", 1883);
		mqttc.setCallback(handleMQTT);

		sprintf(_wdcorrectiontopic, "%s/%s", WS_MQTT_ROOT, WS_WD_CORRECTION_TOPIC);

		_isinit = true;
	}
}

boolean _connected()
{
	mqttInit();

	if (!mqttc.connected())
	{
		mqttc.connect(WiFi.hostname().c_str());
		mqttc.subscribe(_wdcorrectiontopic);
		mqttc.subscribe(SUBS_TEMP_SENSOR_TOPIC);
	}
	return mqttc.connected();
}

void receiveMQTT(void)
{
	if (_connected())
		mqttc.loop();
}

boolean publishMQTT(const char *topic, const char *value)
{
	boolean retval = false;
	char buf[MAX_MQTT_MSG_LEN];

	if (_connected())
	{
		sprintf(buf, "%s/%s", WS_MQTT_ROOT, topic);
		retval = mqttc.publish(buf, value, true);
	}
	return retval;
}