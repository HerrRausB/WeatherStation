#include <ArduinoOTA.h>
#include "gt-wt-03.h"

#include "wsdebug.h"

//---------------------------------------
// internal affairs
//---------------------------------------

const uint32_t _ticksSync = GTWT03_SYNC * TICKS_PER_uS;
const uint32_t _ticksLong = GTWT03_LONG * TICKS_PER_uS;
const uint32_t _ticksShort = GTWT03_SHORT * TICKS_PER_uS;

const uint8_t _syncPulses = GTWT03_SYNC_COUNT * 2;
const uint8_t _msgPulses = GTWT03_MSG_LEN * 2;
const uint8_t _stopPulses = GTWT03_STOP_LEN * 2;
const uint8_t _burstPulses = GTWT03_BURST_LEN * 2;

bool _gtwt03_is_init = false;
volatile bool _txon = false;

uint8_t _txpin = 0,
		_pinstate = 0,
		_txpulse = 0,
		_bursts = 0;

gtwt03_msg_t _msgbuf;
gtwt03_bits_t _msgbits, _bitmask;

uint32_t _lutpulses[_burstPulses];

void IRAM_ATTR _tick(void);

uint32_t _getGTWT03PaylodBits(void);
void _calcChecksum(void);
void _nextBurst(void);
void _makeGTWT03PLUTPulses(void);

void IRAM_ATTR _tick(void)
{
	if (_txpulse < _burstPulses)
	{
		_pinstate = 1 - _pinstate;
		digitalWrite(_txpin, _pinstate);
		timer1_write(_lutpulses[_txpulse]);
		_txpulse++;
	}
	else
	{
		_bursts--;
		_nextBurst();
	}
}

void _nextBurst(void)
{
	if (_bursts)
	{
		_txpulse = 0;
		_pinstate = 0;
		_tick();
	}
	else
	{
		timer1_detachInterrupt();
		digitalWrite(_txpin, LOW);
		_txon = false;
	}
}

uint32_t _getGTWT03PaylodBits()
{
	uint32_t bits = 0;

	bits |= (uint32_t)_msgbuf.id;

	bits <<= GTWT03_HUMD_LEN;
	bits |= (uint32_t)_msgbuf.humidity;

	bits <<= GTWT03_BATT_LEN;
	bits |= (uint32_t)_msgbuf.battery;

	bits <<= GTWT03_BUTN_LEN;
	bits |= (uint32_t)_msgbuf.button;

	bits <<= GTWT03_CHAN_LEN;
	bits |= (uint32_t)_msgbuf.channel;

	bits <<= GTWT03_TEMP_LEN;
	bits |= (uint32_t)_msgbuf.temperature;

	return bits;
}

void _calcChecksum()
{
	uint32_t bits = 0;
	uint16_t key;
	uint8_t data = 0,
			checksum = GTWT03_CHKSUM_INIT,
			*payload;
	int8_t i, k;

	bits = _getGTWT03PaylodBits();
	payload = (uint8_t *)&bits;

	// calculate the checksum - algorithm copied from
	// https://github.com/merbanan/rtl_433/blob/master/src/devices/gt_wt_03.c
	// thanks so far :-)

	for (k = 0; k < 4; ++k)
	{
		data = payload[k];

		key = GTWT03_CHKSUM_KEY;

		for (i = 7; i >= 0; --i)
		{
			// XOR key into sum if data bit is set
			if ((data >> i) & 1)
			{
				checksum ^= (key & 0xff);
			}

			// roll the key right
			key = (key >> 1);
		}
	}
	_msgbuf.checksum = checksum;
}

void _makeGTWT03PLUTPulses()
{
	uint8_t i;
	uint64_t bits = getGTWT03Bits(),
			 m = 1;

	for (i = 0; i < _syncPulses; i++)
	{
		_lutpulses[i] = _ticksSync;
	}

	for (i = 0; i < (_msgPulses); i += 2)
	{
		if (bits & m)
		{
			_lutpulses[_syncPulses + _msgPulses - i - 2] = _ticksLong;
			_lutpulses[_syncPulses + _msgPulses - i - 1] = _ticksShort;
		}
		else
		{
			_lutpulses[_syncPulses + _msgPulses - i - 2] = _ticksShort;
			_lutpulses[_syncPulses + _msgPulses - i - 1] = _ticksLong;
		}

		m <<= 1;
	}

	for (i = 0; i < _stopPulses; i += 2)
	{
		_lutpulses[_burstPulses - _stopPulses + i + 1] = _ticksLong;
		_lutpulses[_burstPulses - _stopPulses + i] = _ticksShort;
	}
}

//---------------------------------------
// API
//---------------------------------------

gtwt03_bits_t getGTWT03Bits()
{
	uint64_t bits;

	bits = (uint64_t)_getGTWT03PaylodBits();

	bits <<= GTWT03_CHKSUM_LEN;
	bits |= (uint64_t)_msgbuf.checksum;

	return bits;
}

void initGTWT03(uint8_t pin)
{
	if (!_gtwt03_is_init)
	{
		_txon = false;
		_txpin = pin;

		_msgbuf.id = 0x2A; // what else? ;-)
		_msgbuf.humidity = 0x00;
		_msgbuf.battery = 0x0;
		_msgbuf.button = 0x0;
		_msgbuf.channel = 0;
		_msgbuf.temperature = 0x000;

		pinMode(_txpin, OUTPUT);

		timer1_detachInterrupt(); // just in case...

		timer1_enable(TIM_DIV16, TIM_EDGE, TIM_SINGLE);

		_gtwt03_is_init = true;
	}
}

void sendGTWT03(float temperature, float humidity, uint8_t channel)
{
	if (_gtwt03_is_init)
	{
		while (_txon)
		{
			ArduinoOTA.handle();
			yield();
			delay(10);
		}
		// scale temperature value

		int16_t _temp = (int16_t)(temperature * GTWT03_TEMP_SCALE);

		// if (_temp < 0)		// convert to 2's complement - obviously not needed due to cast?
		// {
		// 	_temp *= -1;		// remove sign
		// 	_temp ^= 0xFFFF;	// invert bits
		// 	_temp += 1;			// add 1
		// }

#ifdef __DEBUG__
		//char buf[80];
		//sprintf(buf, "T: %.1f, H: %.1f, C: %d", temperature, humidity, channel);
		//wsdebug(buf);
#endif

		_msgbuf.channel = channel -1;
		_msgbuf.temperature = (uint16_t)_temp;
		_msgbuf.humidity = (uint8_t)(humidity + 0.5);

		_calcChecksum();
		_makeGTWT03PLUTPulses();

		_bursts = GTWT03_DEF_BURSTS;
		_txon = true;

		timer1_attachInterrupt(_tick);
		_nextBurst();
	}
}