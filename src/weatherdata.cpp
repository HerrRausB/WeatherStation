#include "weatherdata.h"

uint16_t 		dataUpdateInterval = DEF_DATA_UPDATE_INTERVAL,
				rainTotalSeconds = DEF_TOTAL_RAIN_SECONDS;

weatherdata_t 	data, 
				olddata;

bool init_weather_data = true;

void setDataUpdateInterval (uint16_t intvl)
{
	dataUpdateInterval = intvl;
}

void initWeatherData (void)
{
	memset(&data, 0, sizeof(weatherdata_t));
	memset(&olddata, 0, sizeof(weatherdata_t));
	setDataUpdateInterval (DEF_DATA_UPDATE_INTERVAL);
	init_weather_data = true;
}
