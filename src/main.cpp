#include <EEPROM.h>
#include <ArduinoOTA.h>
#include "weatherstation.h"

// internal prototypes

void _updateResets(void);
void _setupWIFI(void);
void _setupPublish(void);
void _setupI2C(void);

// utility function arrays for cycling through
// functions in setup and loop to ensure, that
// callbacks for OTA etc. are called repeatedly

pvf_t setupFunctions[] =
	{
		_setupWIFI,
		_setupPublish,
		_setupI2C,
		initPublish,
		initI2CInput,
		initISRInput,
		initWeatherData};

pvf_t loopFunctions[] =
	{
		readADC,
		readBME,
		readCCS,
		readLUX,
		calcWindRain,
		publishLight,
		publishWindDir,
		publishWindSpeed,
		publishTempHumRain,
		publishAir,
		publish433MHz};

// global variables

uint16_t connects = 0;

uint32_t past = 0,
		 resets = 0;

const uint8_t numberLoopFunctions = sizeof(loopFunctions) / sizeof(pvf_t);
const uint8_t numberSetupFunctions = sizeof(setupFunctions) / sizeof(pvf_t);

// internal functions

void _resetResets()
{
	EEPROM.begin(512);
	for (uint16_t i = 0; i < 512; i++)
		EEPROM.put(i, 0);
	EEPROM.commit();
	EEPROM.end();
}

void _updateResets()
{
	EEPROM.begin(512);
	EEPROM.get(0, resets);
	resets++;
	EEPROM.put(0, resets);
	EEPROM.commit();
	EEPROM.end();
}

void _setupWIFI()
{

	WiFi.begin(WLAN_SSID, WLAN_PWD);

	while (WiFi.status() != WL_CONNECTED)
		delay(50);

	connects++;

	wsdebug(WiFi.macAddress().c_str());
	wsdebug(WiFi.localIP().toString().c_str());
	wsdebug(WiFi.dnsIP().toString().c_str());
	sprintf(charbuf, "System reset #%d", resets);
	wsdebug(charbuf);
	sprintf(charbuf, "WiFi connect #%d", connects);
	wsdebug(charbuf);

	ArduinoOTA.begin(false);
}

void _setupPublish()
{
	publishMQTT("system/resets", itoa(resets, charbuf, 10));
	publishMQTT("system/connects", itoa(connects, charbuf, 10));
	publishMQTT("system/MAC", WiFi.macAddress().c_str());
	publishMQTT("system/IP", WiFi.localIP().toString().c_str());
	publishMQTT("system/DNS", WiFi.dnsIP().toString().c_str());
	publishMQTT("system/hostname", WiFi.hostname().c_str());
	publishMQTT("system/strength", itoa(getRSSI(WLAN_SSID), charbuf, 10));
}

void _setupI2C()
{
	Wire.begin();

	delay(500);

	// scan for I2C devices...

	int offset = 0;

	memset(charbuf, 0, sizeof(charbuf));
	strcpy(charbuf, "No I2C detected");

	for (int addr = 0x04; addr < 0x78; addr++)
	{
		Wire.beginTransmission(addr);
		if (!Wire.endTransmission())
		{
			sprintf(&charbuf[offset], "0x%X ", addr);
			offset += 5;
		}
	}
	wsdebug(charbuf);
	publishMQTT("system/I2C", charbuf);
}

// standard Arduino stuff

void setup()
{
	// make sure OTA firmware updates are also available during setup

	//_resetResets();
	_updateResets();

	for (uint8_t fidx = 0; fidx < numberSetupFunctions; fidx++)
	{
		setupFunctions[fidx]();

		ArduinoOTA.handle();
		yield();
	}

	past = millis() - dataUpdateInterval * 1000;
}

void loop()
{
	uint32_t now = millis();

	if (WiFi.status() != WL_CONNECTED)
	{
		_setupWIFI();
		_setupPublish();
	}
	else
	{
		ArduinoOTA.handle();
		receiveMQTT();
		yield();

		if ((now - past) >= dataUpdateInterval * 1000)
		{
			past = now;

			// save old data for comparison to avoid
			// publishing unchanged data

			memcpy(&olddata, &data, sizeof(weatherdata_t));

			// cycle through loop functions and repeatedly call
			// external loop functions for OTA and MQTT handling
			//
			// needed to avoid hangups and timeouts when proceeding
			// OTA firmware updates or missing out MQTT control messages
			// as publishing is quite time consuming

			for (uint8_t fidx = 0; fidx < numberLoopFunctions; fidx++)
			{
				loopFunctions[fidx]();

				ArduinoOTA.handle();
				receiveMQTT();
				yield();
			}

			if (init_weather_data)
				init_weather_data = false;
		}
	}
}
