#include "weatherstation.h"

#include "wind_dir_adc.h"

uint8_t _wdcorrection;

const uint8_t 	_lutWDValue[] =	{N,NNO,NO,ONO,O,OSO,SO,SSO,S,SSW,SW,WSW,W,WNW,NW,NNW},
				_wdcount = sizeof(_lutWDValue) / sizeof(uint8_t);
const char * 	_lutWDText[] = 	{"N","NNO","NO","ONO","O","OSO","SO","SSO","S","SSW","SW","WSW","W","WNW","NW","NNW"};
const char * 	_lutWDDeg[] = {"0","22,5","45","67,5","90","112,5","135","157,5","180","202,5","225","247,5","270","292,5","315","337,5"};

const uint8_t 	_lutBFT[] = {1,5,11,19,28,38,49,61,74,88,102,117};
const char * 	_lutBFTText[] =
{
	"Windstille / Flaute",
	"leiser Zug",
	"leichte Brise",
	"schwache Brise",
	"mäßige Brise",
	"frische Brise",
	"starker Wind",
	"steifer Wind",
	"stürmischer Wind",
	"Sturm",
	"schwerer Sturm",
	"orkanartiger Sturm",
	"Orkan"
};

void setWDCorrection (uint16_t degrees)
{
	_wdcorrection = (uint8_t)(degrees / (360 / _wdcount));
}

uint8_t getWDIdx (uint8_t val)
{
	uint8_t idx = sizeof (_lutWDValue) / sizeof(uint8_t);

	while 	(idx && 
				!(
					(val >= (_lutWDValue[idx-1] - WIND_TOLERANCE)) && 
					(val <= (_lutWDValue[idx-1] + WIND_TOLERANCE))
				)
			) idx--;

	// correct idx
	if (idx) 
	{
		idx--;
		idx += _wdcorrection;
		idx %= _wdcount;
		idx++;
	}

	return idx;
}

const char * getWDText (uint8_t val)
{
	uint8_t idx = getWDIdx (val);

	return idx ? _lutWDText[idx-1] : "-";
}

const char * getWDDeg (uint8_t val)
{
	uint8_t idx = getWDIdx (val);

	return idx ? _lutWDDeg[idx-1] : "-";
}

uint8_t kmh2bft (float kmh)
{	
	uint8_t bft = (sizeof (_lutBFT) / sizeof (uint8_t));

	while (bft && (_lutBFT[bft-1] > kmh)) bft--;

	return bft;
}

const char * getBFTText (uint8_t bft)
{
	
	if (bft < (sizeof (_lutBFT) / sizeof (uint8_t) + 1) && (bft >= 0))
		return _lutBFTText[bft];
	else
		return "-";
}
