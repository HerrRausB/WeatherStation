#include "i2c-input.h"
#include "publish.h"
#include "mqtt.h"
#include "lut.h"

// I2C devices - thanks to Adafruit! :-)

Adafruit_BME280 BME;
Adafruit_PCF8591 ADC;
Adafruit_CCS811 CCS;
BH1750 LUX;

bool hasBME280 = false,
	 hasPCF8591 = false,
	 hasCCS811 = false,
	 hasBH1750 = false;

uint8_t _avgAnalogRead(uint8_t channel, uint8_t count)
{
	uint8_t c = count > 0 ? count : 1;
	uint16_t sum = 0;
	for (uint8_t i = 0; i < c; i++)
		sum += ADC.analogRead(channel);
	return (uint8_t)(sum / c);
}

void initI2CInput()
{
	if (ADC.begin())
	{
		sprintf(charbuf, "0x%02X", PCF8591_DEFAULT_ADDR);
		publishMQTT("system/PCF8591", charbuf);
		hasPCF8591 = true;
	}
	else
	{
		publishMQTT("system/PCF8591", "none");
	}

	// set up I2C BME280 temperature / pressure / humidity sensor

	if (BME.begin(BME280_ADDRESS_ALTERNATE))
	{
		sprintf(charbuf, "0x%02X", BME280_ADDRESS_ALTERNATE);
		publishMQTT("system/BME", charbuf);
		hasBME280 = true;
	}
	else
	{
		publishMQTT("system/BME", "none");
	}

	// set up I2C CCS811 air quality sensor

	if (CCS.begin())
	{
		sprintf(charbuf, "0x%02X", CCS811_ADDRESS);
		publishMQTT("system/CCS811", charbuf);
		hasCCS811 = true;

		// //calibrate temperature sensor as in Adafruit's example
		// delay (10);
		// while(!CCS.available());
		// float temp = CCS.calculateTemperature();
		// CCS.setTempOffset(temp - 25.0);
	}
	else
	{
		publishMQTT("system/CCS811", "none");
	}

	// set up BH1750 light sensor

	if (LUX.begin())
	{
		sprintf(charbuf, "0x%02X", LUX.getAddr());
		publishMQTT("system/BH1750", charbuf);
		hasBH1750 = true;

		// //calibrate temperature sensor as in Adafruit's example
		// delay (10);
		// while(!CCS.available());
		// float temp = CCS.calculateTemperature();
		// CCS.setTempOffset(temp - 25.0);
	}
	else
	{
		publishMQTT("system/BH1750", "none");
	}
}

void readADC()
{
	if (hasPCF8591)
	{
		ADC.begin();

		// data.solar = ADC.analogRead(0);
		data.solar = _avgAnalogRead(0, 5);

		//uint8_t adc_dir = _avgAnalogRead(1, 5);
		 uint8_t adc_dir = ADC.analogRead(1);
		if (getWDIdx(adc_dir))
			data.wind.dir = adc_dir;

		// data.photo = ADC.analogRead(2);
		data.photo = _avgAnalogRead(2, 5);
	}
}

void readBME()
{
	if (hasBH1750)
	{
		data.BME.temp = adjustFloat(BME.readTemperature(), 1);
		data.BME.pres = (uint16_t)((BME.readPressure() + 50.0) / 100);
		data.BME.hum = adjustFloat(BME.readHumidity(), 1);
	}
}

void readCCS()
{
	if (hasCCS811 && CCS.available())

		// set compensation values for humidty and temeprature
		// CCS.setEnvironmentalData (data.BME.hum, data.BME.temp);

		if (!CCS.readData())
		{
			data.CCS.co2 = CCS.geteCO2();
			data.CCS.tvoc = CCS.getTVOC();
		}
}

void readLUX()
{
	if (hasBH1750 && LUX.measurementReady())
		data.LUX = adjustFloat(LUX.readLightLevel(), 1);
}
