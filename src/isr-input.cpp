#include <Arduino.h>
#include "isr-input.h"
#include "util.h"


uint16_t			windTicks = 0;
uint8_t				rainTicks = 0;
uint32_t	        rainDebounceMillis = 0;

// rolling buffer to calculate hourly rain amount 					
uint16_t			bufidx = 0,
					idxRainTicks = 0,
					numRainTicks = 0,
					sizeRainTicks = 0;
uint8_t				* bufRainTicks = NULL;

// internal function prototypes

void IRAM_ATTR _isrAnemometerTick (void);
void IRAM_ATTR _isrRainGaugeTick (void);
void _allocRainTicksBuffer (void);

// internal functions

void IRAM_ATTR _isrAnemometerTick ()
{
	windTicks++;
}

void IRAM_ATTR _isrRainGaugeTick ()
{
	if ((millis() - rainDebounceMillis) >= RAINGAUGE_DEBOUNCE)
	{
		rainTicks++;
		rainDebounceMillis = millis ();
	}
}

void _allocRainTicksBuffer ()
{
	sizeRainTicks = rainTotalSeconds / dataUpdateInterval;


	if ((bufRainTicks = (uint8_t *)malloc (sizeRainTicks * sizeof(uint8_t))) > 0)
	{
		memset (bufRainTicks, 0, sizeRainTicks);
	}
}

// API

void initISRInput ()
{
	// init wind speed measurements

	pinMode (ANEMOMETER_PIN, INPUT_PULLUP);
	attachInterrupt (digitalPinToInterrupt (ANEMOMETER_PIN), _isrAnemometerTick, FALLING);

	// init rain level measurement
	
	numRainTicks = 0;
	_allocRainTicksBuffer ();
   	rainDebounceMillis = millis() - RAINGAUGE_DEBOUNCE;
	pinMode (RAINGAUGE_PIN, INPUT_PULLUP);
	attachInterrupt (digitalPinToInterrupt (RAINGAUGE_PIN), _isrRainGaugeTick, FALLING);
}

void calcWindRain ()
{
	uint16_t 	tempWindTicks = 0,
				tempRainTicks = 0;

	// calculate wind speed

	tempWindTicks = windTicks;
	windTicks = 0;
	data.wind.ticks = tempWindTicks;
	data.wind.speed = adjustFloat ((tempWindTicks * ANEMOMETER_KMH_TICK) / (dataUpdateInterval), 2);

	// calculate rain level

	bufRainTicks[idxRainTicks] = rainTicks;
	rainTicks = 0;
	idxRainTicks++;

	// amount of valid tick recordings in rolling buffer
	// doesn't change when rolling buffer filled,
	// used to calculate evaluated time frame as
	// specified in rainTotalSeconds from weatherdata.cpp/h
	// see also _allocRainTicksBuffer
	if (numRainTicks < sizeRainTicks) numRainTicks++;

	// as it is a rolling buffer...
	idxRainTicks %= sizeRainTicks;

	// sum up all rain ticks - invalid values in rolling
	// buffer will be 0 and won't affect the sum
	for (bufidx = 0; bufidx < numRainTicks; bufidx++) tempRainTicks += bufRainTicks[bufidx];
	data.rain.ticks = tempRainTicks;

	// calculate time amount of valid ticks
	data.rain.seconds = numRainTicks * dataUpdateInterval;
	data.rain.mm = adjustFloat (((float)data.rain.ticks * RAINGAUGE_MM_TICK), 3);
}	