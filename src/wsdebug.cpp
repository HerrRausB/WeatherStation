#include "weatherstation.h"

#ifdef __DEBUG__

#include <WiFiUdp.h>

IPAddress	ip;
WiFiUDP		udpConnection;

void wsdebug (int dbg)
{
	char buf[10];
	wsdebug (itoa (dbg, buf, 10));
}

void wsdebug (const char * dbg)
{
	//if (udpConnection.beginPacket (ip.fromString(WCDBG_TARGET), WCDBG_PORT))

	//while (!udpConnection.availableForWrite())
	//	delay (50);

	if (udpConnection.beginPacket (IPAddress(192,168,0,10), 6666))
	{
		udpConnection.write (dbg);
		udpConnection.write ("\n");
		udpConnection.endPacket();
	}
}

#else

#define wsdebug(x)

#endif