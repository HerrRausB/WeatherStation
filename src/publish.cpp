#include "publish.h"
#include "lut.h"
#include "i2c-input.h"

#include "wsdebug.h"

uint32_t lastPublish433 = 0;

void initPublish()
{
	initGTWT03(GTWT03_PIN);
	lastPublish433 = millis() - GTWT03_PUBLISH_ITVL * 1000;
}

void publishLight()
{
	if (init_weather_data || (data.solar != olddata.solar))
	{
		sprintf(charbuf, "0x%0X (%d)", data.solar, data.solar);
		publishMQTT("light/solar/adc", charbuf);

		sprintf(charbuf, "%d", data.solar * 100 / ADC_MAX);
		publishMQTT("light/solar/percent", charbuf);

		sprintf(charbuf, "%0.2f", solar2volt(data.solar));
		publishMQTT("light/solar/volt", charbuf);
	}

	if (init_weather_data || (data.photo != olddata.photo))
	{
		sprintf(charbuf, "0x%0X (%d)", data.photo, data.photo);
		publishMQTT("light/photo/adc", charbuf);

		sprintf(charbuf, "%d", INVADC(data.photo) * 100 / ADC_MAX);
		publishMQTT("light/photo/percent", charbuf);
	}

	if (init_weather_data || (data.LUX != olddata.LUX))
	{
		sprintf(charbuf, "%0.1f", data.LUX);
		publishMQTT("light/lux", charbuf);
	}
}

void publishWindDir()
{
	if (init_weather_data || (data.wind.dir != olddata.wind.dir))
	{
		sprintf(charbuf, "%0X", data.wind.dir);
		publishMQTT("wind/dir/adc/hex", charbuf);

		sprintf(charbuf, "%d", data.wind.dir);
		publishMQTT("wind/dir/adc/dec", charbuf);

		sprintf(charbuf, "%d", getWDIdx(data.wind.dir));
		publishMQTT("wind/dir/idx", charbuf);

		sprintf(charbuf, "%s", getWDText(data.wind.dir));
		publishMQTT("wind/dir/text", charbuf);

		sprintf(charbuf, "%s", getWDDeg(data.wind.dir));
		publishMQTT("wind/dir/deg", charbuf);
	}
}

void publishWindSpeed()
{
	if (init_weather_data || (data.wind.speed != olddata.wind.speed))
	{
		uint8_t bft = kmh2bft(data.wind.speed);

		sprintf(charbuf, "%0.2f", ((float)data.wind.ticks / (float)(UPDATE_INTERVAL / 1000)));
		publishMQTT("wind/speed/ticks", charbuf);

		sprintf(charbuf, "%0.2f", data.wind.speed);
		publishMQTT("wind/speed/kmh", charbuf);

		sprintf(charbuf, "%0.2f", kmh2mph(data.wind.speed));
		publishMQTT("wind/speed/mph", charbuf);

		sprintf(charbuf, "%0.2f", kmh2knt(data.wind.speed));
		publishMQTT("wind/speed/kn", charbuf);

		sprintf(charbuf, "%d", bft);
		publishMQTT("wind/speed/bft", charbuf);

		publishMQTT("wind/speed/text", getBFTText(bft));
	}
}

void publishTempHumRain()
{
	if (init_weather_data || (data.BME.temp != olddata.BME.temp))
	{
		sprintf(charbuf, "%0.1f", data.BME.temp);
		publishMQTT("temp/C", charbuf);

		sprintf(charbuf, "%0.1f", C2F(data.BME.temp));
		publishMQTT("temp/F", charbuf);
	}

	if (init_weather_data || (data.BME.hum != olddata.BME.hum))
	{
		sprintf(charbuf, "%0.1f", data.BME.hum);
		publishMQTT("humidity/percent", charbuf);
	}

	if (init_weather_data ||
		(data.rain.ticks != olddata.rain.ticks) ||
		(data.rain.seconds != olddata.rain.seconds))
	{
		sprintf(charbuf, "%d", data.rain.ticks);
		publishMQTT("rain/ticks", charbuf);

		sprintf(charbuf, "%d", data.rain.seconds);
		publishMQTT("rain/seconds", charbuf);

		sprintf(charbuf, "%0.2f", data.rain.mm);
		publishMQTT("rain/mm", charbuf);
	}
}

void publishAir()
{
	if (init_weather_data || (data.BME.pres != olddata.BME.pres))
	{
		sprintf(charbuf, "%d", data.BME.pres);
		publishMQTT("pressure/mbar", charbuf);
	}
	if (init_weather_data || (data.CCS.co2 != olddata.CCS.co2))
	{
		sprintf(charbuf, "%d", data.CCS.co2);
		publishMQTT("air/CO2", charbuf);
	}
	if (init_weather_data || (data.CCS.tvoc != olddata.CCS.tvoc))
	{
		sprintf(charbuf, "%d", data.CCS.tvoc);
		publishMQTT("air/TVOC", charbuf);
	}
}

void publish433MHz()
{
#ifndef __DEV__
	uint32_t now = millis();

	if ((now - lastPublish433) >= GTWT03_PUBLISH_ITVL * 1000)
	{
		sendGTWT03(data.BME.temp, data.BME.hum, GTWT03_CHANNEL_WS);
		// sendGTWT03(23.0, 42.0, GTWT03_CHANNEL_EXT);
		lastPublish433 = now;
	}
#endif
}