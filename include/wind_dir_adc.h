#pragma once

#define N 0xC5	 // decimal 197 .
#define NNO 0x66 // decimal 102 .
#define NO 0x74	 // decimal 116 .
#define ONO 0x4D // decimal 77 .
#define O 0x53	 // decimal 83 .
#define OSO 0x1d // decimal 29 .
#define SO 0x2F	 // decimal 47 .
#define SSO 0x20 // decimal 32 .
#define S 0x48	 // decimal 72 .
#define SSW 0x3E // decimal 62 .
#define SW 0x9F	 // decimal 159 .
#define WSW 0x96 // decimal 150 .
#define W 0xEC	 // decimal 236 .
#define WNW 0xCF // decimal 207 .
#define NW 0xDE	 // decimal 222 .
#define NNW 0x80 // decimal 128 .
