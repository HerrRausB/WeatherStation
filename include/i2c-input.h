#ifndef __I2C_INPUT_H__
#define __I2C_INPUT_H__

#include <Adafruit_BME280.h>
#include <Adafruit_PCF8591.h>
#include <Adafruit_CCS811.h>
#include <BH1750.h>

#include "weatherdata.h"

#define UPDATE_INTERVAL 5000

#define ADC_MAX 0xFF
#define INVADC(x) (ADC_MAX - (x))

// global variables

extern Adafruit_BME280 BME;
extern Adafruit_PCF8591 ADC;
extern Adafruit_CCS811 CCS;
extern BH1750 LUX;

void initI2CInput(void);
void readADC(void);
void readBME(void);
void readCCS(void);
void readLUX(void);
void calcWindRain(void);

#endif
