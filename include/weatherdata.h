#ifndef __WEATHERDATA_H__
#define __WEATHERDATA_H__

#include "weathertypes.h"

#define DEF_DATA_UPDATE_INTERVAL    5           // seconds
#define DEF_TOTAL_RAIN_SECONDS      3600        // seconds

extern uint16_t				dataUpdateInterval, // seconds
							rainTotalSeconds;   // seconds

extern weatherdata_t 		data,
							olddata;

extern bool init_weather_data;						

void setDataUpdateInterval (uint16_t intvl);
void initWeatherData (void);                            

#endif 