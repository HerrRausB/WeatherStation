#ifndef __WEATHERTYPES_H__
#define __WEATHERTYPES_H__

#include <stdint.h>

#include <Adafruit_BME280.h>
#include <Adafruit_PCF8591.h>
#include <Adafruit_CCS811.h>

// pointer to void function

typedef void (* pvf_t) (void);

// element of void function pointer list

typedef struct _lpvf_t 
{
	pvf_t	f;
	_lpvf_t	* next;
}
lpvf_t;

// weather data

typedef struct 
{
	uint8_t solar;
	uint8_t photo;
	struct 
	{
		uint32_t ticks;
		uint32_t seconds;
		float mm;
	}
	rain;
	struct 
	{
		uint8_t dir;
		uint32_t ticks;
		float speed;
	} wind;
	struct 
	{
		float temp;
		uint16_t pres;
		float hum;
	} BME;
	struct 
	{
		uint16_t co2;
		uint16_t tvoc;
	} CCS;
	float LUX;
	
}
weatherdata_t;

#endif