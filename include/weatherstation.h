#ifndef __WEATHER_STATION_H__
#define __WEATHER_STATION_H__

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Wire.h>

#include "weatherdata.h"

#include "util.h"
#include "i2c-input.h"
#include "isr-input.h"
#include "publish.h"
#include "lut.h"
#include "config.h"
#include "mqtt.h"
#include "wsdebug.h"

#if not(defined(WLAN_SSID) || defined(WLAN_PWD))
	#error You have to define WLANSSID="\"<SSID>\"" and WLANPWD="\"<PWD>\"" as external environment variables!
#endif

#endif