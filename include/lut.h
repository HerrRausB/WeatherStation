#ifndef __LUT_H__
#define __LUT_H__

#include <stdint.h>

/*
 * ADC values for solar input voltages
 * 
 *  0V - 0x00
 *  1V - 0x14
 *  2V - 0x28
 *  3V - 0x3C
 *  4V - 0x51
 *  5V - 0x65
 *  6V - 0x78
 *  7V - 0x8E
 *  8V - 0xA4
 *  9V - 0xBA
 * 10V - 0xCF
 * 11V - 0xE3
 * 12V - 0xF8
 * 
 */

#define WIND_STEPS		16
#define WIND_TOLERANCE	1
#define SOLAR_1V		0x14
#define SOLAR_12V		0xF8
#define solar2volt(x)	((float)((float)x/(float)SOLAR_1V))

void setWDCorrection (uint16_t degrees);
uint8_t getWDIdx (uint8_t val);
const char * getWDText (uint8_t val);
const char * getWDDeg (uint8_t val);

uint8_t kmh2bft (float kmh);
const char * getBFTText (uint8_t bft);

#endif