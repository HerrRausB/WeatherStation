#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <stdint.h>

typedef struct 
{
	uint8_t wdcorrection;
}
wsconfig_t;

extern wsconfig_t _config;

#endif 