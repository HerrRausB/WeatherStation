#ifndef __MQTT_H__
#define __MQTT_H__

#include <string.h>
#include <Ethernet.h>
#include <PubSubClient.h>
#include <Arduino_JSON.h>

#define MAX_MQTT_MSG_LEN 256
#define MAX_MQTT_TOPIC_LEN 256

#ifndef WS_MQTT_ROOT
#define WS_MQTT_ROOT "balcony/weather"
#endif

#define SUBS_TEMP_SENSOR_TOPIC "zigbee2mqtt/TMP006"

#define WS_WD_CORRECTION_TOPIC "control/wdcorrection"

void receiveMQTT(void);
boolean publishMQTT(const char *topic, const char *value);

#endif