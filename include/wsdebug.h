#ifndef __WSDEBUG_H__
#define __WSDEBUG_H__

#ifdef __DEBUG__
#ifndef WCDBG_TARGET
	#define WCDBG_TARGET	"192.168.0.3"
#endif
#ifndef WCDBG_PORT
	#define WCDBG_PORT 		6666
#endif
	void wsdebug (int dbg);
	void wsdebug (const char * dbg);
#else
	#define wsdebug(x)
#endif

#endif
