#ifndef __UTIL_H__
#define __UTIL_H__

#include <stdint.h>

#define C2F(x) (((x)*9/5)+32)
#define kmh2mph(x) ((float)(x/1.6))
#define kmh2knt(x) ((float)(x/1.852))

extern char charbuf[256];

int32_t getRSSI(const char* target_ssid);
float adjustFloat (float f, uint8_t d);

#endif 