#ifndef __ISR_INPUT_H__
#define __ISR_INPUT_H__

#include "weatherdata.h"

#define ANEMOMETER_PIN		D6
#define ANEMOMETER_KMH_TICK	((float)2.3872)

#define RAINGAUGE_PIN		D5
#define RAINGAUGE_MM_TICK   ((float)0.2794)
#define RAINGAUGE_DEBOUNCE	5

extern uint16_t				windTicks;
extern uint8_t				rainTicks;

void initISRInput (void);
void calcWindRain (void);
#endif 