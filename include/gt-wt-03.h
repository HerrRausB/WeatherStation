#ifndef __GTWT03_H__
#define __GTWT03_H__

#include <Arduino.h>
#include <stdint.h>

#define TICKS_PER_uS 5 // TIM_DIV16

#define GTWT03_SYNC 855
#define GTWT03_LONG 610
#define GTWT03_SHORT 245

#define GTWT03_ID_LEN 8
#define GTWT03_BATT_LEN 1
#define GTWT03_BUTN_LEN 1
#define GTWT03_CHAN_LEN 2
#define GTWT03_TEMP_LEN 12
#define GTWT03_HUMD_LEN 8
#define GTWT03_CHKSUM_LEN 8
#define GTWT03_STOP_LEN 1

#define GTWT03_SYNC_COUNT 4

#define GTWT03_CHKSUM_KEY 0x3100
#define GTWT03_CHKSUM_INIT 0x2D

#define GTWT03_MSG_LEN (GTWT03_ID_LEN +   \
						GTWT03_HUMD_LEN + \
						GTWT03_BATT_LEN + \
						GTWT03_BUTN_LEN + \
						GTWT03_CHAN_LEN + \
						GTWT03_TEMP_LEN + \
						GTWT03_CHKSUM_LEN)

#define GTWT03_BURST_LEN (GTWT03_SYNC_COUNT + \
						  GTWT03_MSG_LEN +    \
						  GTWT03_STOP_LEN)

#define GTWT03_DEF_BURSTS 23

#define GTWT03_TEMP_SCALE 10

typedef struct
{
	uint8_t id : GTWT03_ID_LEN;
	uint8_t humidity : GTWT03_HUMD_LEN;
	uint8_t battery : GTWT03_BATT_LEN;
	uint8_t button : GTWT03_BUTN_LEN;
	uint8_t channel : GTWT03_CHAN_LEN;
	uint16_t temperature : GTWT03_TEMP_LEN;
	uint8_t checksum : GTWT03_CHKSUM_LEN;
} gtwt03_msg_t;

typedef uint64_t gtwt03_bits_t;

void initGTWT03(uint8_t pin);
gtwt03_bits_t getGTWT03Bits(void);
void getGTWT03BitString(char *buf, uint16_t len);
void sendGTWT03(float temeperature, float humidity, uint8_t channel = 1);

#endif