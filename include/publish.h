#ifndef __PUBLISH_H__
#define __PUBLISH_H__

#include "util.h"
#include "mqtt.h"
#include "gt-wt-03.h"

#define GTWT03_PIN D7
#define GTWT03_CHANNEL_WS 2
#define GTWT03_CHANNEL_EXT 3
#define GTWT03_PUBLISH_ITVL 10

void initPublish(void);
void publishLight(void);
void publishWindDir(void);
void publishWindSpeed(void);
void publishTempHumRain(void);
void publishAir(void);
void publish433MHz(void);

#endif