#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

static uint8_t chk_rollbyte(uint8_t const message[], unsigned bytes, uint16_t gen)
{
	int i;
	unsigned k;
	uint8_t data;
	uint16_t key;
    uint8_t sum = 0;

	printf ("chk_rollbyte\n");

    for (k = 0; k < bytes; ++k) {
        data = message[bytes - 1 - k];
		data ^= 0xFF;
        key = gen;
		printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, sum);
        for (i = 7; i >= 0; --i) {
			printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, sum);
            // XOR key into sum if data bit is set
            if ((data >> i) & 1)
			{
                sum ^= key & 0xff;
				printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, sum);
			}
			printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, sum);

            // roll the key right
            key = (key >> 1);
			printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, sum);
        }
    }
	printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, sum);
    return sum;
}

static uint8_t my_checksum(uint8_t const message[], unsigned bytes, uint16_t gen)
{
	uint16_t 	key;
	uint8_t 	data,
				checksum;
	int8_t		i,
				k;

	printf ("my_checksum\n");

	for (k = 0; k < bytes; ++k)
	{
		data = message[k];

		key = gen;

		printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, checksum);

		for (i = 7; i >= 0; --i)
		{
			printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, checksum);
			
			// XOR key into sum if data bit is set
			
			if ((data >> i) & 1)
			{
				checksum ^= (key & 0xff);
				printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, checksum);
			}

			printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, checksum);

			// roll the key right
			key = (key >> 1);
			
			printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, checksum);
		}
	}

	printf ("i:%02d k:%02d key:0x%04X data:0x%02X chk:0x%02X\n", i, k, key, data, checksum);

	return checksum ^ 0x2D;
}

int main (void)
{

	uint32_t bits = 0b10111000001010100000000011101011;

	printf ("result:0x%02X\n", my_checksum ((uint8_t *)&bits, 4, 0x3100));

	printf ("result:0x%02X\n", chk_rollbyte ((uint8_t *)&bits, 4, 0x3100));
}