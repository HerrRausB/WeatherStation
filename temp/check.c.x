#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


#define GTWT03_ID_LEN		8
#define GTWT03_BATT_LEN		1
#define GTWT03_BUTN_LEN		1
#define GTWT03_CHAN_LEN		2
#define GTWT03_TEMP_LEN		12
#define GTWT03_HUMD_LEN		8
#define GTWT03_CHKSUM_LEN	8

#define GTWT03_SYNC_COUNT	4

#define GTWT03_MSG_LEN (GTWT03_ID_LEN + \
						GTWT03_HUMD_LEN + \
						GTWT03_BATT_LEN + \
						GTWT03_BUTN_LEN + \
						GTWT03_CHAN_LEN + \
						GTWT03_TEMP_LEN + \
						GTWT03_CHKSUM_LEN)			

typedef struct
{
	uint8_t id : GTWT03_ID_LEN;
	uint8_t humidity : GTWT03_HUMD_LEN;
	uint8_t battery : GTWT03_BATT_LEN;
	uint8_t button : GTWT03_BUTN_LEN;
	uint8_t channel : GTWT03_CHAN_LEN;
	uint16_t temperature : GTWT03_TEMP_LEN;
	uint8_t checksum : GTWT03_CHKSUM_LEN;
}
gtwt03_msg_t;

void check2comp (char * val)
{
	printf ("val = %s\n", val);

	int16_t _temp = (int16_t)(atof (val) * 10);

	printf ("_temp = 0x%04X\n", _temp);

	if (_temp < 0)			// zweierkomplement erzeugen
	{
		_temp *= -1;		// vorzeichen entfernen
		_temp ^= 0xFFFF;	// bits invertieren
		_temp += 1;			// 1 addieren
	}

	printf ("_temp = 0x%04X\n", _temp);
}

void checkcast ()
{
	gtwt03_msg_t 	_msgbuf;
	uint32_t		bits = 0;
	uint8_t			* bytebuffer, i, j, offset = 0;
	char 			buf[10];

	_msgbuf.id = 0x98;
	_msgbuf.humidity = 0x76;
	_msgbuf.battery = 0x0;
	_msgbuf.button = 0x0;
	_msgbuf.channel = 0x1;
	_msgbuf.temperature = 0xDEF;

	bits |= ((uint32_t)_msgbuf.id) << offset;

	offset += GTWT03_ID_LEN;
	bits |= ((uint32_t)_msgbuf.humidity) << offset;

	offset += GTWT03_HUMD_LEN;
	bits |= (((uint32_t)_msgbuf.temperature) >> 8) << offset;

	offset += 4;
	bits |= ((uint32_t)_msgbuf.channel) << offset;

	offset += GTWT03_CHAN_LEN;
	bits |= ((uint32_t)_msgbuf.button) << offset;

	offset += GTWT03_BUTN_LEN;
	bits |= ((uint32_t)_msgbuf.battery) << offset;

	offset += GTWT03_BATT_LEN;
	bits |= (((uint32_t)_msgbuf.temperature) & 0xFF) << offset;


	// bits |= (uint32_t)_msgbuf.id;

	// bits <<= GTWT03_HUMD_LEN;
	// bits |= (uint32_t)_msgbuf.humidity;

	// bits <<= GTWT03_BATT_LEN;
	// bits |= (uint32_t)_msgbuf.battery;

	// bits <<= GTWT03_BUTN_LEN;
	// bits |= (uint32_t)_msgbuf.button;

	// bits <<= GTWT03_CHAN_LEN;
	// bits |= (uint32_t)_msgbuf.channel;

	// bits <<= GTWT03_TEMP_LEN;
	// bits |= (uint32_t)_msgbuf.temperature;

	bytebuffer = (uint8_t *)&bits;

	// j = bytebuffer[0];
	// bytebuffer[0] = bytebuffer[3];
	// bytebuffer[3] = j;
	// j = bytebuffer[1];
	// bytebuffer[1] = bytebuffer[2];
	// bytebuffer[2] = j;

	for (i = 0; i < 4; i++)	
		printf ("byte %d: 0x%02X\n", i, bytebuffer[i]);

}

int main (int argc, char * argv[])
{
	if (argc > 1) check2comp (argv[1]);

	checkcast ();

	return 0;
}
