#include <stdio.h>
#include <string.h>
#include <stdint.h>

#ifndef bool
	#define bool uint8_t
	#define true 1
	#define false 0
#endif

#define PROLOGUE_LOW_ZEROS	4
#define PROLOGUE_HIGH_ZEROS	8
#define PROLOGUE_SYNC_ZEROS 18

uint16_t 	len = 0, 
			zerocnt = 0;
bool 		foundone = false;

void eval ()
{
	if (zerocnt == PROLOGUE_LOW_ZEROS)
	{
		putchar ('0');
	}
	else if (zerocnt == PROLOGUE_HIGH_ZEROS)
	{
		putchar ('1');
	}
	foundone = false;
	zerocnt = 0;
}	
	
int main (uint16_t argc, uint8_t * argv[])
{
	if (argc > 1)
	{
		len = strlen (argv[1]);

		for (int i; i < len; i++)
		{
			switch (argv[1][i])
			{
				case '0' :
				{
					if (foundone) zerocnt++;
					break;
				}
				case '1' :
				{
					if (foundone) eval ();
					foundone = true;
					break;
				}
				default :
				{
					/* NOP */;
				}
			}
		}
		eval ();
	}
	return 0;
}