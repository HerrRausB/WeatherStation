#include <stdio.h>
#include <string.h>
#include <stdint.h>


///////////////////////////
// GT-WT-03 signal decoder for UHR
///////////////////////////

#ifndef bool
	#define bool uint8_t
	#define true 1
	#define false 0
#endif

int main (uint16_t argc, uint8_t * argv[])
{
	if (argc > 1)
	{
		uint16_t len = strlen (argv[1]);

		for (uint16_t i = 0; i < len; i+=4)
		{
			if (!strncmp (&(argv[1][i]), "1000", 4)) putc('0', stdout);
			if (!strncmp (&(argv[1][i]), "1110", 4)) putc('1', stdout);
		}
	}
	printf ("\n");
	return 0;
}